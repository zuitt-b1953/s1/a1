package com.zuitt.wdc044.models;

import javax.persistence.*;



@Entity
@Table(name="users")
public class User {
//User model properties:
    //Id - Long/Primary key/Auto-increments
    //username - String
    //password - String
    //Getters and setters for username and password
    //Getter for id
   @Id
   @GeneratedValue
    private long id;

   public long getId(){
       return id;
   }

   @Column
    private String username;

   @Column
    private String password;

   public User(){}

    public User(String username, String password){
       this.username = username;
       this.password = password;
    }

    public String getUsername(){
       return username;
    }

    public void setUsername(String username){
       this.username = username;
    }

    public String getPassword(){
       return password;
    }

    public void setPassword(String password){
       this.password = password;
    }



}
