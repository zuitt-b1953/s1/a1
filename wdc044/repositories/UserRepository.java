package com.zuitt.wdc044.repositories;

import com.zuitt.wdc044.models.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends CrudRepository<User, Object> {
    //User model properties:
    //Id - Long/Primary key/Auto-increments
    //username - String
    //password - String
    //Getters and setters for username and password
    //Getter for id
}
